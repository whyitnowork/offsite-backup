variable "vault_token" {}

provider "vault" {
address = ""
skip_tls_verify = false
token = "${var.vault_token}"
}

data "vault_generic_secret" "terraform-secrets" {
  path = "terraform-secrets/login"
}

## VSphere Bits
provider "vsphere" {
  user           = "${data.vault_generic_secret.terraform-secrets.data["ad_user"]}"
  password       = "${data.vault_generic_secret.terraform-secrets.data["ad_password"]}"
  vsphere_server = ""
  # If you have a self-signed cert
  allow_unverified_ssl = false
}

#Data Sources
data "vsphere_datacenter" "dc" {
  name = "Homelab"
}
data "vsphere_datastore" "datastore" {
  name          = "Primus"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}
data "vsphere_compute_cluster" "cluster" {
  name          = "Clusteroony"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}
data "vsphere_network" "network" {
  name          = "A-LAN"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}
data "vsphere_virtual_machine" "template" {
  name          = "debian-11-golden"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

# New VM
resource "vsphere_virtual_machine" "offsite-backup" {
  name             = "offsite-backup"
  resource_pool_id = "${data.vsphere_compute_cluster.cluster.resource_pool_id}"
  datastore_id     = "${data.vsphere_datastore.datastore.id}"
  num_cpus = 2
  memory   = 4096
  guest_id = "${data.vsphere_virtual_machine.template.guest_id}"
  scsi_type = "${data.vsphere_virtual_machine.template.scsi_type}"
  firmware = "${data.vsphere_virtual_machine.template.firmware}"
  network_interface {
    network_id   = "${data.vsphere_network.network.id}"
    adapter_type = "vmxnet3"
  }
  disk {
    label            = "disk0"
    size             = "${data.vsphere_virtual_machine.template.disks.0.size}"
    eagerly_scrub    = "${data.vsphere_virtual_machine.template.disks.0.eagerly_scrub}"
    thin_provisioned = "${data.vsphere_virtual_machine.template.disks.0.thin_provisioned}"
  }
  clone {
    template_uuid = "${data.vsphere_virtual_machine.template.id}"
    customize {
      linux_options {
        host_name = "offsite-backup"
        domain    = ""
      }
      network_interface {
        ipv4_address = "10.10.10.56"
        ipv4_netmask = 24
      }
      ipv4_gateway = "10.10.10.1"
      dns_suffix_list = [""]
      dns_server_list = ["10.10.10.10","10.10.10.11"]
    }
  }
}

