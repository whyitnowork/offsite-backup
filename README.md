# borg-backup-script

### Description
Dev ops?

Was fun to get this working. Write up here: https://whyitno.work/automating-my-offsite-backup/

### Notes
Var Locations:

.env
terraform/main.tf


### Useful commands
#### For copying authorised_keys to the hetner borg repo

$ echo -e "mkdir .ssh \n chmod 700 .ssh \n put authorized_keys .ssh/authorized_keys \n chmod 600 .ssh/authorized_keys" | sftp <storage-box-username>@<storage-box-username>.your-storagebox.de

#### Connect to the remote borg repo via sftp. Assumes ssh key has been mounted in ssh-agent

$ sftp -P 23 <storage-box-username>@<storage-box-username>.your-storagebox.de

#### Add borg passhphrease to env vars

$ export BORG_PASSPHRASE=""

#### Borg commands

$ borg create ssh://<storage-box-username>@<storage-box-username>.your-storagebox.de:23/./backup/::2017_11_11_initial

$ borg init --encryption=repokey ssh://<storage-box-username>@<storage-box-username>.your-storagebox.de:23/./backup/
