from flask import Flask
from flask import jsonify
import requests
from datetime import datetime
import os
from git import Repo
import git
import gotify
import shutil
from decouple import config
import sys

DIRS_TO_CHECK = ["/mnt/documents/", "/mnt/offsite/"]

REPO_DIRECTORY = config('GITLAB_LOCAL_CACHE')

GITLAB_API_ENDPOINT = "{url}/api/v4/users/{token_owner_id}/projects?access_token={token}&per_page={results}".format(
    url=config('GITLAB_URL'),
    token_owner_id=config('GITLAB_TOKEN_OWNER_ID'),
    token=config('GITLAB_TOKEN'),
    results=config('GITLAB_RESULTS_PER_PAGE')
)

GOTIFY_INFO = config('GOTIFY_INFO')
GOTIFY_FAIL = config('GOTIFY_FAIL')
GOTIFY_SUCCESS = config('GOTIFY_SUCCESS')

VAULT_TOKEN=config('VAULT_TOKEN')
BORG_REMOTE_USERNAME=config('BORG_REMOTE_USERNAME')
BORG_REMOTE_REPO=config('BORG_REMOTE_REPO')
GOTIFY_URL = config('GOTIFY_URL')
GOTIFY_TOKEN = config('GOTIFY_TOKEN')

DEBUG = True

gotify_obj = gotify.gotify(
    base_url = GOTIFY_URL,
    app_token = GOTIFY_TOKEN,
)


def purge_local_repo_cache():
    if DEBUG == True:
        print("Purging local repos")
    try:
        shutil.rmtree(REPO_DIRECTORY)
        os.mkdir(REPO_DIRECTORY)
    except:
        os.remove(REPO_DIRECTORY)
        os.mkdir(REPO_DIRECTORY)

def git_clone_initial(response):
    if DEBUG == True:
        print("Performing initial clone")
    try:
        for key in response.json():
            if DEBUG == True:
                print (key["ssh_url_to_repo"])
            if key["name"]:
                Repo.clone_from(key["ssh_url_to_repo"], REPO_DIRECTORY + key["name"])
    except Exception as e:
        gotify_obj.create_message(
            str(e),
            title=GOTIFY_FAIL,
        priority=0)

def git_pull_latest():
    if DEBUG == True:
        print("Pulling latest repos")
    my_list = os.listdir(REPO_DIRECTORY)
    for i in my_list:
        repo = git.Repo(REPO_DIRECTORY + i)
        repo.remotes.origin.pull()

def perform_daily_backup():
    if DEBUG == True:
        print("Performing borg remote backup")
    try:
        for d in DIRS_TO_CHECK:
            dirs = os.listdir(d)
            print(d)
            print (len(dirs))
            if len(dirs) == 0:
                 # send failure alert and end run
                print("gotify backup dir")
                gotify_obj.create_message(
                    "Backup directory: {dir_being_checked} is empty".format(dir_being_checked=str(d)),
                    title=GOTIFY_FAIL,
                    priority=0)
                quit()

    # borg backup the directories
        command="borg create --stats --list ssh://{username}@{username}.your-storagebox.de:23/./{repo}/::{timestamp} /mnt/".format(
            username = BORG_REMOTE_USERNAME,
            repo = BORG_REMOTE_REPO,
            timestamp=datetime.now().strftime("%Y%m%d-%H%M%S")
         )
        os.system(command)

    # send notification
    except Exception as e:
        gotify_obj.create_message(
            str(e),
            title=GOTIFY_FAIL,
        priority=0)

def main():
    try:
        dt = datetime.now()
        gotify_obj.create_message(
            "Started at: {time}".format(time=dt),
            title=GOTIFY_INFO,
        priority=0)

        if DEBUG == True:
            print("Getting api response from gitlab")
            print(GITLAB_API_ENDPOINT)
        
        response = requests.get(GITLAB_API_ENDPOINT, verify=True)
        total_repos = (len(response.json()))
        # print ("remote repos: {remote}".format(remote=total_repos))
        dirs = os.listdir(REPO_DIRECTORY)
        # print (dirs)
        cached_repos = len(dirs)
        print ("cached repos: {cached}".format(cached=cached_repos))

        if cached_repos == 0:
            git_clone_initial(response)
        elif cached_repos == total_repos:
            #print ("skipping pulling for now")
            git_pull_latest()
        else:
            purge_local_repo_cache()
            git_clone_initial(response)
        
        perform_daily_backup()

        dt = datetime.now()
        gotify_obj.create_message(
            "Successfully finished at: {time}".format(time=dt),
            title=GOTIFY_SUCCESS,
        priority=0)

    except Exception as e:
        gotify_obj.create_message(
            str(e),
            title=GOTIFY_FAIL,
        priority=0)

if __name__ == "__main__":
    main()