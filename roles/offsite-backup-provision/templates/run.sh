#!/bin/sh
export BORG_PASSPHRASE="{{ lookup('hashi_vault', 'secret=ansible-secrets/data/borg-backup:borg_repo_password') }}" && \
echo $BORG_PASSPHRASE && \
eval $(ssh-agent) && \
ssh-add /home/deploy/.ssh/borg-ssh && \
ssh-add /home/deploy/.ssh/gitlab-ssh && \
python3 /home/deploy/python-script/borg-backup.py